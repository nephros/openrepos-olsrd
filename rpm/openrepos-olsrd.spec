# 
# Do NOT Edit the Auto-generated Part!
# Generated by: spectacle version 0.32
# 

Name:       openrepos-olsrd

# >> macros
# << macros
%define upstream_name olsrd

Summary:    OLSRd Routing Daemon
Version:    0.9.8
Release:    5
Group:      Applications/Internet
License:    BSD
URL:        https://github.com/OLSR/olsrd
Source100:  openrepos-olsrd.yaml
Patch0:     Makefile-cp-busybox.patch
Requires:   bash
Requires:   coreutils
Requires:   iproute
Requires:   iptables
BuildRequires:  bison
BuildRequires:  flex
BuildRequires:  gcc
BuildRequires:  make
BuildRequires:  sed
BuildRequires:  desktop-file-utils
Provides:   olsrd

%description
OLSRd is an implementation of the Ad-Hoc routing protocol OLSR (RFC3626).
It provides (multihop) routing in a dynamic, changing Ad-Hoc network,
either wired, wireless, or both.
This version supports both IPv4 and IPv6.
See http://www.olsr.org/ for more info.

Please edit /etc/olsrd/olsrd.conf to suit your system.
Run 'systemctl enable olsrd' to enable automatic starting of OLSRd.
Run 'systemctl restart olsrd' to start OLSRd.


%package plugins-all
Summary:    Meta package to install all plugins
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   openrepos-olsrd-plugin-arprefresh
Requires:   openrepos-olsrd-plugin-bmf
Requires:   openrepos-olsrd-plugin-dot-draw
Requires:   openrepos-olsrd-plugin-dyngw
Requires:   openrepos-olsrd-plugin-dyngw-plain
Requires:   openrepos-olsrd-plugin-jsoninfo
Requires:   openrepos-olsrd-plugin-mdns
Requires:   openrepos-olsrd-plugin-nameservice
Requires:   openrepos-olsrd-plugin-netjson
Requires:   openrepos-olsrd-plugin-p2pd
Requires:   openrepos-olsrd-plugin-pgraph
Requires:   openrepos-olsrd-plugin-poprouting
Requires:   openrepos-olsrd-plugin-quagga
Requires:   openrepos-olsrd-plugin-secure
Requires:   openrepos-olsrd-plugin-sgwdynspeed
Requires:   openrepos-olsrd-plugin-txtinfo
Requires:   openrepos-olsrd-plugin-watchdog

%description plugins-all
Meta package depending on all olsr plugins

%package plugin-arprefresh
Summary:    The plugin caches MAC addresses of OLSRd neighbors
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   olsrd = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-arprefresh
The plugin listens to all received UDP packets on port 698 and maintains
an internal list of MAC addresses extracted from these. The kernel ARP
cache will be refreshed from this list if a direct-neighbor host route is
configured. Result: no more ARP lookups if you use a larger routing
chain - e.g. fetch a web site 8 olsr-hops away does not show the typical
8-nodes-need-to-ARP first delay.
IPv4 only.
Does not support VLANs.


%package plugin-bmf
Summary:    The plugin floods IP-multicast and IP-local-broadcast traffic over an OLSRd network.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   olsrd = 0.9.8
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-bmf
The Basic Multicast Forwarding Plugin floods IP-multicast and
IP-local-broadcast traffic over an OLSRd network. It uses the
Multi-Point Relays (MPRs) as identified by the OLSR protocol
to optimize the flooding of multicast and local broadcast packets
to all the hosts in the network. To prevent broadcast storms, a
history of packets is kept; only packets that have not been seen
in the past 3-6 seconds are forwarded.


%package plugin-dot-draw
Summary:    The plugin generates topology information in the dot format.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   olsrd = 0.9.8
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-dot-draw
The plugin generates output (over a TCP-IPv4 socket) in the dot format.
The dot tool generates visual presentations of directed graphs.
It can be downloaded as part of the GraphViz package from:
http://www.graphviz.org/

telnet to 127.0.0.1 port 2004 to receive the data


%package plugin-dyngw
Summary:    The plugin checks if the local node has an Internet connection.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   olsrd = 0.9.8
Requires:   iputils
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-dyngw
Plugin is IPv4 only and it only runs on Linux with the libpthread
library installed!

This is a plugin that checks if the local node has a Internet-
connection. A Internet-connetion is identified by a "default gw" with a
hopcount of 0. That is a route to 0.0.0.0/0 with metric 0.  By default
the check is done every 5 secs. You can change the check interval by
providing an value for "PingInterval" in the plugin's section of olsrd.conf.

If one or more IPv4 addresses are given as values for "Ping" in the
section or dyn_gw in olsrd.conf, then a test is done to validate if
there is really an internet connection (and not just an entry in the
routing table). If any of the arbitrary many given IPv4 addresses can be
pinged, the validation was successful. The addresses are pinged in the
order given in the olsrd.conf (i.e. the first given address is pinged
first, the the 2nd, and so on). For this to work a command like
"ping -c 1 -q <PING-ADDRESS>" must be possible on the system OLSRd runs
on. The validation is based on the return value of this ping command.

Since OLSR uses hopcount/metric on all routes this plugin will
not respond to Internet gateways added by OLSRd.

When a Internet gateway is discovered - this node will start
announcing 0.0.0.0/0 connectivity by HNA messages flooded into
the OLSR network. If the route is removed the HNA messages
will not be transmitted. This check is totally dynamic and
Internet connectivity might come and go.

This plugin can also process more specific routes by specifying groups of HNAs
(Host and Network Association) and ping hosts.


%package plugin-dyngw-plain
Summary:    Automatically adds a default gateway HNA when the node has an Internet connection.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   olsrd = 0.9.8
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-dyngw-plain
Automatically adds a default gateway HNA when the node has such a route.
Automatically removes a default gateway HNA when the node has no such route.
IPv4 only.

This plugin is without Ping/libthread. It is the plain dyn_gw!


%package plugin-jsoninfo
Summary:    The plugin delivers runtime status and configuration of OLSRd as json.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   olsrd = 0.9.8
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-jsoninfo
The jsoninfo plugin is an info plugin.

An info plugin aims to deliver all of the information about the runtime status
and configuration of OLSRd.

Information that can be requested:
* specific parts of runtime data or startup configuration
* overview of runtime data
* overview of startup configuration
* all of the information in one report

Additionally, an info plugin can dump the current OLSRd configuration in the
in the olsrd.conf format.

==================
SUPPORTED COMMANDS
==================

Grouped information:
* /all
* /runtime
* /startup

Runtime information:
* /neighbors
* /links
* /routes
* /hna
* /mid
* /topology
* /gateways
* /interfaces
* /2hop
* /sgw

A special case for Freifunk, combining /neighbors and /links:
* /neighbours

Start-up information:
* /version
* /config
* /plugins

The current configuration, formatted for writing directly to a configuration
file, like /etc/olsrd/olsrd.conf:
* /olsrd.conf


%package plugin-mdns
Summary:    The plugin distributes multicast DNS messages over an OLSRd network.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   olsrd = 0.9.8
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-mdns
This plugin goal is the distribution of multicast DNS messages over an OLSR
Wireless Mesh Network.

In a wireless mesh network, the usage of wireless interfaces in ad-hoc mode
and the OLSR routing protocol prevent multicast messages to be distributed
all over the network.

We are especially interested in the distribution of Multicast DNS (mDNS)
messages, used for host-based service discovery, over the networks that do
not directly partecipate in the OLSR mesh cloud.

This task is achieved in the following way:
 1. the local router picks up from the local non-OLSR (HNA) network mDNS
    messages and encapsulates them in a new type of OLSR messages,
 2. the OLSR infrastructure is exploited for the transport of these messages,
 3. remote routers decapsulate mDNS messages from received OLSR messages and
    send them over their attached non-OLSR networks.

The work could have its practical and immediate application in all the
wireless network communities that employ the OLSR protocol.


%package plugin-nameservice
Summary:    A simple DNS replacement for OLSR networks.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   olsrd = 0.9.8
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-nameservice
A simple DNS replacement for OLSR networks.

This plugin distributes host name (DNS) information over OLSR. Every
node which runs the olsr daemon can announce it's own name, names for
other IP addresses it is associated with (HNAs) and whether it is running
a "real" nameserver which can resolve other (internet) names (upstream
DNS server).

The nodes in the network collect this information and write the host
names to a file (possibly /etc/hosts), and the 3 nearest upstream
nameservers to another file (possibly /etc/resolv.conf).

These files can be used to resolve hostnames on the local system and/or
be read by a DNS server like "dnsmasq", to make the names available
via the ordinary DNS protocol to other clients as well.


%package plugin-netjson
Summary:    The plugin delivers topology information of OLSRd as json.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   olsrd = 0.9.8
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-netjson
The netjson plugin is an info plugin.

An info plugin aims to deliver all of the information about the runtime status
and configuration of OLSRd.

Information that can be requested:
* specific parts of runtime data or startup configuration
* overview of runtime data
* overview of startup configuration
* all of the information in one report

Additionally, an info plugin can dump the current OLSRd configuration in the
in the olsrd.conf format.

==================
SUPPORTED COMMANDS
==================

* /NetworkRoutes
* /NetworkGraph
* /DeviceConfiguration (not currently supported)
* /DeviceMonitoring    (not currently supported)
* /NetworkCollection


%package plugin-p2pd
Summary:    The plugin distributes Peer to Peer Discovery messages over an OLSRd network.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   olsrd = 0.9.8
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-p2pd
This plugin goal is the distribution of Peer to Peer Discovery messages over an
OLSR Wireless Mesh Network.

In a wireless mesh network, the usage of wireless interfaces in ad-hoc mode and
the OLSR routing protocol prevent UDP messages to be distributed all over the
network.

We are especially interested in the distribution of UDP messages, used for host-
based service discovery, over the networks that do not directly participate in
the OLSR mesh cloud.

This task is achieved in the following way:
 1. the local router picks up from the local non-OLSR (HNA) network UDP messages
    and encapsulates them in a new type of OLSR messages,
 2. the OLSR infrastructure is exploited for the transport of these messages,
 3. remote routers decapsulate UDP messages from received OLSR messages and send
    them over their attached non-OLSR networks.

The work could have its practical and immediate application in wireless network
communities that employ the OLSR protocol.


%package plugin-pgraph
Summary:    generates topology information.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   olsrd = 0.9.8
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-pgraph
The plugin generates topology information.

%package plugin-poprouting
Summary:    The plugin can get and set the values of the Hello and the TC timers/multiplier.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   olsrd = 0.9.8
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-poprouting
The poprouting plugin is used to set the values of the Hello and the TC
timers/multiplier in OLSRd. It can be also used to get the current values from
the daemon.

==================
SUPPORTED COMMANDS
==================
The commands can be specified with or without a float number after the =.
In the former case the timer or the multiplier will be set at that specific
value. In the latter case the plugin will return the actual value for that
timer or multiplier.

* /helloTimer=%f
* /TCTimer=%f
* /helloTimerMult=%f
* /TCTimerMult=%f
* /helloTimer
* /TCTimer
* /helloTimerMult
* /TCTimerMult


%package plugin-quagga
Summary:    The plugin allows OLSRd to redistribute routes via quagga.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-quagga
This is the Quagga Plugin for OLSRd.
It allows OLSRd to redistribute from various quagga-protocols
as well as to export olsr-routes to quagga so that they can be
redistributed by the quagga-routing-daemons.


%package plugin-secure
Summary:    The plugin encrypts all traffic on an OLSRd network.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires:   openssl-devel
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-secure
The plugin encrypts all traffic on an OLSRd network, and uses a shared secret
key for signature generation and verification. For nodes to participate in the
OLSR routing domain they need to use the key used by the other nodes.


%package plugin-sgwdynspeed
Summary:    allow dynamic adjustments of the default gateway speed.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-sgwdynspeed
The plugin allow dynamic adjustments of the default gateway speed.

%package plugin-txtinfo
Summary:    The plugin delivers runtime status and configuration of OLSRd as text.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-txtinfo
The jsoninfo plugin is an info plugin.

An info plugin aims to deliver all of the information about the runtime status
and configuration of OLSRd.

Information that can be requested:
* specific parts of runtime data or startup configuration
* overview of runtime data
* overview of startup configuration
* all of the information in one report

Additionally, an info plugin can dump the current OLSRd configuration in the
in the olsrd.conf format.

==================
SUPPORTED COMMANDS
==================

Grouped information:
* /all
* /runtime
* /startup

Runtime information:
* /nei
* /lin
* /rou
* /hna
* /mid
* /top
* /gat
* /int
* /2ho
* /sgw

A special case for Freifunk, combining /nei and /lin:
* /neighbours

Start-up information:
* /ver
* /config  (not supported, will output nothing)
* /plugins (not supported, will output nothing)

The current configuration, formatted for writing directly to a configuration
file, like /etc/olsrd/olsrd.conf:
* /con


%package plugin-watchdog
Summary:    The plugin lets an external program detect an OLSRd hang.
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description plugin-watchdog
This plugin is used for detecting a total freeze of the OLSRd daemon by an
external script. Once per timeinterval (configurable) it writes the current
time into a file.


%package devel
Summary:    Development files for %{name}
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}

%description devel
Development files for %{name}.

%package docs
Summary:    Documentation files for %{name}
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}

%description docs
Documentation files for %{name}.

%prep
# Makefile-cp-busybox.patch
%patch0 -p1
# >> setup
# << setup

%build
# >> build pre
# << build pre



# >> build post
%{__make} -j 1 prefix=/usr DESTDIR="%{buildroot}" OS="linux" $_ARCH NOSTRIP=1 NORPATH=1 DEBUG="0" VERBOSE=1 SANITIZE_ADDRESS=0 SANITIZE_LEAK=0 SANITIZE_UNDEFINED=0 SUBDIRS="olsrd arprefresh bmf dot_draw dyn_gw dyn_gw_plain info jsoninfo mdns nameservice netjson poprouting p2pd pgraph quagga secure sgwdynspeed txtinfo watchdog"
# << build post

%install
rm -rf %{buildroot}
# >> install pre
# << install pre

# >> install post
%{__make} -j 1 prefix=/usr DESTDIR="%{buildroot}" OS="linux" $_ARCH NOSTRIP=1 NORPATH=1 DEBUG="0" VERBOSE=1 SANITIZE_ADDRESS=0 SANITIZE_LEAK=0 SANITIZE_UNDEFINED=0 SUBDIRS="arprefresh bmf dot_draw dyn_gw dyn_gw_plain info jsoninfo mdns nameservice netjson poprouting p2pd pgraph quagga secure sgwdynspeed txtinfo watchdog" install_all
cp -r redhat/etc redhat/usr "%{buildroot}"/
%{__install} -p -D -m 644 files/olsrd.conf.funkfeuer $RPM_BUILD_ROOT/%{_sysconfdir}/olsrd/olsrd.conf.funkfeuer
%{__install} -p -D -m 644 rpm/olsrd.conf.sailfishos $RPM_BUILD_ROOT/%{_sysconfdir}/olsrd/olsrd.conf.sailfishos
# rename binary
%{__install} -p -D -m 755 olsrd $RPM_BUILD_ROOT/%{_sbindir}/%{name}
rm -f $RPM_BUILD_ROOT/%{_sbindir}/%{upstream_name}
rm -r $RPM_BUILD_ROOT/%{_sysconfdir}/default
rm -f $RPM_BUILD_ROOT/%{_unitdir}/olsrd.service
# 64bit system: libdir is /usr/lib64, but make install hardcodes /usr/lib, lets hardcode this:
%{__install} -p -D -m 644  rpm/%{name}.service $RPM_BUILD_ROOT/%{_unitdir}/%{name}.service
%{__install} -p -D -m 644  rpm/%{name}-txtinfo.desktop  $RPM_BUILD_ROOT/%{_datadir}/applications/%{name}-txtinfo.desktop
sed -i -e "s#@@BINNAME@@#%{name}#g" $RPM_BUILD_ROOT/%{_unitdir}/%{name}.service
# correct some paths
sed -i -e 's@#!/usr/bin/bash@#!/usr/bin/env bash@' $RPM_BUILD_ROOT/%{_sbindir}/sgw_policy_routing_setup.sh
# add icons
for r in 512 256 128 86; do
%{__install} -p -D -m 644 rpm/%{name}_${r}.png $RPM_BUILD_ROOT/%{_datadir}/icons/hicolor/${r}x${r}/apps/%{name}.png
done

# << install post

desktop-file-install --delete-original       \
  --dir %{buildroot}%{_datadir}/applications             \
   %{buildroot}%{_datadir}/applications/*.desktop

%post plugin-arprefresh -p /sbin/ldconfig

%postun plugin-arprefresh -p /sbin/ldconfig

%post plugin-bmf -p /sbin/ldconfig

%postun plugin-bmf -p /sbin/ldconfig

%post plugin-dot-draw -p /sbin/ldconfig

%postun plugin-dot-draw -p /sbin/ldconfig

%post plugin-dyngw -p /sbin/ldconfig

%postun plugin-dyngw -p /sbin/ldconfig

%post plugin-dyngw-plain -p /sbin/ldconfig

%postun plugin-dyngw-plain -p /sbin/ldconfig

%post plugin-jsoninfo -p /sbin/ldconfig

%postun plugin-jsoninfo -p /sbin/ldconfig

%post plugin-mdns -p /sbin/ldconfig

%postun plugin-mdns -p /sbin/ldconfig

%post plugin-nameservice -p /sbin/ldconfig

%postun plugin-nameservice -p /sbin/ldconfig

%post plugin-netjson -p /sbin/ldconfig

%postun plugin-netjson -p /sbin/ldconfig

%post plugin-p2pd -p /sbin/ldconfig

%postun plugin-p2pd -p /sbin/ldconfig

%post plugin-pgraph -p /sbin/ldconfig

%postun plugin-pgraph -p /sbin/ldconfig

%post plugin-poprouting -p /sbin/ldconfig

%postun plugin-poprouting -p /sbin/ldconfig

%post plugin-quagga -p /sbin/ldconfig

%postun plugin-quagga -p /sbin/ldconfig

%post plugin-secure -p /sbin/ldconfig

%postun plugin-secure -p /sbin/ldconfig

%post plugin-sgwdynspeed -p /sbin/ldconfig

%postun plugin-sgwdynspeed -p /sbin/ldconfig

%post plugin-txtinfo -p /sbin/ldconfig

%postun plugin-txtinfo -p /sbin/ldconfig

%post plugin-watchdog -p /sbin/ldconfig

%postun plugin-watchdog -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%config %{_sysconfdir}/olsrd/olsrd.conf
%config %{_sysconfdir}/olsrd/olsrd.conf.funkfeuer
%config %{_sysconfdir}/olsrd/olsrd.conf.sailfishos
%{_sbindir}/%{name}
%{_sbindir}/sgw_policy_routing_setup.sh
%{_unitdir}/%{name}.service
%ghost %{_rundir}/%{name}.pid
%{_datadir}/icons/hicolor/*/apps/%{name}.png
# >> files
# << files

%files plugins-all
%defattr(-,root,root,-)
# >> files plugins-all
# << files plugins-all

%files plugin-arprefresh
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_arprefresh.so.0.1
%{_docdir}/olsrd/README_ARPREFRESH
# >> files plugin-arprefresh
# << files plugin-arprefresh

%files plugin-bmf
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_bmf.so.1.7.0
%{_docdir}/olsrd/README_BMF
# >> files plugin-bmf
# << files plugin-bmf

%files plugin-dot-draw
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_dot_draw.so.0.3
%{_docdir}/olsrd/README_DOT_DRAW
%{_docdir}/olsrd/olsr-topology-view.pl
# >> files plugin-dot-draw
# << files plugin-dot-draw

%files plugin-dyngw
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_dyn_gw.so.0.5
%{_docdir}/olsrd/README_DYN_GW
# >> files plugin-dyngw
# << files plugin-dyngw

%files plugin-dyngw-plain
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_dyn_gw_plain.so.0.4
%{_docdir}/olsrd/README_DYN_GW_PLAIN
# >> files plugin-dyngw-plain
# << files plugin-dyngw-plain

%files plugin-jsoninfo
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_jsoninfo.so.1.1
%{_docdir}/olsrd/README_JSONINFO
# >> files plugin-jsoninfo
# << files plugin-jsoninfo

%files plugin-mdns
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_mdns.so.1.0.1
%{_docdir}/olsrd/README_MDNS
# >> files plugin-mdns
# << files plugin-mdns

%files plugin-nameservice
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_nameservice.so.0.4
%{_docdir}/olsrd/README_NAMESERVICE
# >> files plugin-nameservice
# << files plugin-nameservice

%files plugin-netjson
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_netjson.so.1.1
%{_docdir}/olsrd/README_NETJSON
# >> files plugin-netjson
# << files plugin-netjson

%files plugin-p2pd
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_p2pd.so.0.1.0
%{_docdir}/olsrd/README_P2PD
# >> files plugin-p2pd
# << files plugin-p2pd

%files plugin-pgraph
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_pgraph.so.1.1
%{_docdir}/olsrd/README_PGRAPH
# >> files plugin-pgraph
# << files plugin-pgraph

%files plugin-poprouting
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_poprouting.so.1.0
%{_docdir}/olsrd/README_POPROUTING
# >> files plugin-poprouting
# << files plugin-poprouting

%files plugin-quagga
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_quagga.so.0.2.2
%{_docdir}/olsrd/README_QUAGGA
%{_docdir}/olsrd/quagga-0.98.6.diff
%{_docdir}/olsrd/quagga-0.99.21.diff
# >> files plugin-quagga
# << files plugin-quagga

%files plugin-secure
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_secure.so.0.6
%{_docdir}/olsrd/README_SECURE
# >> files plugin-secure
# << files plugin-secure

%files plugin-sgwdynspeed
%defattr(-,root,root,-)
%config %{_sysconfdir}/olsrd/olsrd.sgw.speed.conf
%{_exec_prefix}/lib/olsrd_sgwdynspeed.so.1.0.0
%{_docdir}/olsrd/README_SGWDYNSPEED
# >> files plugin-sgwdynspeed
# << files plugin-sgwdynspeed

%files plugin-txtinfo
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_txtinfo.so.1.1
%{_datadir}/applications/%{name}-txtinfo.desktop
%{_docdir}/olsrd/README_TXTINFO
# >> files plugin-txtinfo
# << files plugin-txtinfo

%files plugin-watchdog
%defattr(-,root,root,-)
%{_exec_prefix}/lib/olsrd_watchdog.so.0.1
%{_docdir}/olsrd/README_WATCHDOG
# >> files plugin-watchdog
# << files plugin-watchdog

%files devel
%defattr(-,root,root,-)
# >> files devel
# << files devel

%files docs
%defattr(-,root,root,-)
# >> files docs
%{_mandir}/*
%{_docdir}/olsrd/*
# << files docs
